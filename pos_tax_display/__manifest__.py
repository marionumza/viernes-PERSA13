# -*- coding: utf-8 -*-
{
    'name': "POS Tax Display",
    'version': '1',
    'summary': """
        This module allows you to show tax on POS order lines.

        """,
    'author': 'WebVeer',
    'category': 'Point Of Sale',
    
    'website': '',

    'depends': ['point_of_sale'],
    'data': [
        ],
    'qweb': [
        'static/src/xml/pos.xml',
    ],
    'images': ['static/description/tax.jpg'],
    'price': 5.00,
    'currency': 'EUR',
    'installable': True,
}
