from odoo import models, api

from odoo.addons.l10n_ca_check_printing.models.account_payment import account_payment 

@api.multi
def do_print_checks(self):
    if self:
        check_layout = self[0].company_id.account_check_printing_layout
        return self.env.ref('l10n_ca_check_printing.%s' % check_layout).report_action(self)
    return super(account_payment, self).do_print_checks()

account_payment.do_print_checks = do_print_checks

    
