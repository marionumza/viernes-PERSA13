{
    'name': 'Xmarts Invoice Odoo ',
    'version': '11.0.3',
    'category': 'Sales Management',
    'description': """ get data from sale order and set in the Invoice.
    """,
    'author':'Axel',
    'depends': ['sale','account'],
    'data': [
	'views/Contacto_Payment_USO.xml',
'views/invoice_Payment_USO.xml',

	  
    ],
    'qweb': [
        ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}
