from odoo import api, fields, models, _


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    
    @api.depends('lot_ids','product_id')
    def _get_related_product_lot(self):
        for line in self:
            lot_id = self.env['stock.production.lot'].search([('product_id', '=', line.product_id.id)])
            lot = lot_id.filtered(lambda lot:lot.product_qty > 0 and lot.product_id.id == line.product_id.id)
            quant_obj = self.env['stock.quant'].search([('location_id.usage','=','internal'),('lot_id','in',lot.ids)])
            quant_id = quant_obj.filtered(lambda qt:qt.reserved_quantity == 0.00 and qt.quantity != 0)
            lot_ids = quant_id.mapped('lot_id').ids
            line.related_lot_ids = [(6,0,lot_ids)] 
        
    
    lot_ids = fields.Many2many('stock.production.lot',string='Lot', copy=False)
    related_lot_ids = fields.Many2many('stock.production.lot','stock_production_lot_order_line','lot_id','order_line_id',string='Lot', compute='_get_related_product_lot')
    
    @api.onchange('lot_ids')
    def onchange_lot(self):
        self.product_uom_qty = 0.0
        for lot in self.lot_ids:
            self.product_uom_qty += lot.product_qty 

    @api.multi
    def _prepare_invoice_line(self, qty):
        res = super(SaleOrderLine, self)._prepare_invoice_line(qty)        
        if self.lot_ids:            
            res.update({'lot_ids' : [(6, 0, self.lot_ids.ids)]})
        return res

    @api.multi
    @api.onchange('product_id')
    def product_id_change(self):
        result = super(SaleOrderLine, self).product_id_change()
        lot_id = self.env['stock.production.lot'].search([('product_id', '=', self.product_id.id)])
        lot = lot_id.filtered(lambda lot:lot.product_qty > 0 and lot.product_id.id == self.product_id.id)
        quant_obj = self.env['stock.quant'].search([('location_id.usage','=','internal'),('lot_id','in',lot.ids)])
        quant_id = quant_obj.filtered(lambda qt:qt.reserved_quantity == 0.00 and qt.quantity != 0)
        result['domain'].update({'lot_ids': [('id', 'in', quant_id.mapped('lot_id').ids)]})
        return result
